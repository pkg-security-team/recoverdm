recoverdm (0.20-9) unstable; urgency=medium

  * Team upload.
  * d/p/*: Add 50_ftbfs-with-gcc-14.patch (Closes: #1075441).
  * d/u/metadata: Rework metadata to comply with DEP-12.
  * d/control: Bump Standards-Version to 4.7.0.
  * d/copyright: Add missing and new copyright holders.
  * d/salsa-ci.yml: Add variable SALSA_CI_DISABLE_BUILD_PACKAGE_ALL.

 -- Sven Geuer <sge@debian.org>  Fri, 16 Aug 2024 16:26:16 +0200

recoverdm (0.20-8) unstable; urgency=medium

  * Team upload.

  [ Joao Eriberto Mota Filho ]
  * debian/control:
      - Bumped Standards-Version to 4.6.0.1.
      - Migrated DH level to 13.
  * debian/copyright:
      - Added licensing for debian/manpage/create-man.sh.
      - Updated packaging copyright years.
  * debian/manpage/:
      - Switched genallman.sh to create-man.sh (from txt2man).
      - Removed files *.header, no longer needed.
      - Regenerated the manpages.
  * debian/upstream/metadata: removed fields about sites no longer available.
  * debian/watch: added a fake site to explain about the current status of the
    original upstream homepage. The original site no longer exists.

  [ Debian Janitor ]
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Set field Upstream-Contact in debian/copyright.
  * Use secure URI in Homepage field.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 20 Sep 2021 20:47:44 -0300

recoverdm (0.20-7) unstable; urgency=medium

  [ Thiago Andrade Marques ]
  * Team upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.5.0.
  * debian/copyright: added packaging rights to me and Samuel Henrique.
  * debian/patches/40_dev-c.patch: added to fix a notice of failure to declare
    a function in the build.
  * debian/rules: added dh_verbose.
  * debian/watch: added the GitHub as source.

  [ Samuel Henrique ]
  * Team upload.
  * Add salsa-ci.yml.
  * Configure git-buildpackage for Debian.

 -- Thiago Andrade Marques <thmarques@gmail.com>  Sat, 29 Feb 2020 16:24:03 -0300

recoverdm (0.20-6) unstable; urgency=medium

  * Team upload.
  * Add fix for mergebad crash (Closes: #716182).
  * d/control:
    - Drop empty Uploaders field.
    - Bump std-version to 4.3.0.
  * d/watch: switch to secure url.
  * d/tests: add smoke tests as autopkgtest testsuite.
  * d/upstream/metadata: update metadata.
  * d/patches: tag patches as forwarded.

 -- Aleksey Kravchenko <rhash.admin@gmail.com>  Fri, 18 Jan 2019 12:36:12 +0300

recoverdm (0.20-5) unstable; urgency=medium

  * Team upload.
  [ Raphaël Hertzog ]
  * d/control:
    - Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org
    - Update team maintainer address to Debian Security Tools

  [ SZ Lin (林上智) ]
  * Add upstream metadata file
  * d/control:
    - Bump debhelper version to 11
    - Bump Standards-Version to 4.2.1
  * d/copyright:
    - Fix insecure copyright format URI
   d/compat:
    - Bump compat version to 11

 -- SZ Lin (林上智) <szlin@debian.org>  Mon, 10 Sep 2018 13:08:59 +0800

recoverdm (0.20-4) unstable; urgency=medium

  * Team upload.
  * Bumped DH level to 10.
  * debian/control:
      - Bumped Standards-Version to 3.9.8.
      - Updated the Vcs-Git field to use https instead of git.
  * debian/copyright: updated the packaging copyright years.
  * debian/patches/:
      - fix-makefile: renamed to 10_fix-makefile.patch.
      - 20_fix-typo-binary.patch: added to fix a typo in final binary.
  * debian/watch: bumped to version 4.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 25 Jan 2017 21:18:18 -0200

recoverdm (0.20-3) unstable; urgency=medium

  * Team upload.
  * Migrations:
      - debian/copyright to 1.0 format.
      - debian/rules to new (reduced) format.
      - DebSrc to 3.0.
      - DH level to 9.
  * debian/control:
      - Bumped Standards-Version to 3.9.6.
      - Fixed a typo in long description. Thanks to
        Davide Prina <davide.prina@gmail.com>. (Closes: #589398)
      - Improved the short and long descriptions.
      - Removed quilt from Build-Depends field. DH 9 no longer needs this.
      - Updated the Vcs-* fields.
  * debian/dirs: added to create a needed directory.
  * debian/manpage/:
      - Created to gather the new manpages.
      - Written new manpages.
  * debian/manpages: updated to install the new manpages.
  * debian/patches/:
      - Removed 01-manpage-removal.patch and 02-manpage-typo.patch. The
        manpages where re-written.
      - fix-makefile: added to provide GCC hardening and to fix the path to
        install.
  * debian/recoverdm.install: removed. The upstream makefile will install the
      files.
  * debian/recoverdm.manpages: renamed to manpages.
  * debian/source.lintian-override: removed. Using 'Team upload' now.
  * debian/watch: created.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 26 Jul 2015 20:09:06 -0300

recoverdm (0.20-2) unstable; urgency=low

  * Updating package to standards version 3.8.2.
  * Adding lintian overrides.
  * Using correct rfc-2822 date formats in changelog.

 -- Daniel Baumann <daniel@debian.org>  Thu, 09 Jul 2009 12:21:32 +0200

recoverdm (0.20-1) unstable; urgency=low

  * Prefixing debhelper files with package name.
  * Merging upstream version 0.20.
  * Using quilt rather than dpatch.
  * Updating to standards 3.8.1.
  * Updating year in copyright file.
  * Re-adding manpage from recoverdm 0.19 which was removed by upstream in
    version 0.20 (for unknown reason; probably mistake).

 -- Daniel Baumann <daniel@debian.org>  Thu, 28 May 2009 21:56:15 +0200

recoverdm (0.19-2) unstable; urgency=low

  * Adding patch from A. Costa <agcosta@gis.net> to fix spelling errors in
    manpage (Closes: #504541).

 -- Daniel Baumann <daniel@debian.org>  Thu, 06 Nov 2008 16:16:00 +0100

recoverdm (0.19-1) unstable; urgency=low

  [ Christophe Monniez ]
  * Initial release (Closes: #469062).
  * Bumping package to debhelper 6
  * Moved from admin section to utils
  * Changed architecture from i386 to any
  * Changed copyright to machine readable format
  * Changed package priority from extra to optional
  * Added a homepage field in the control file
  * Added maintainer and uploader field in control
  * Bumped standards-version in control to 3.7.3
  * Added an install rule in the original Makefile
  * Added a man page for recoverdm
  * Various debian/rules cleanup and reordering
  * Added a man page for mergebad

  [ Daniel Baumann ]
  * Removing useless whitespaces.
  * Some minor cleanups.
  * Rewrapping long description in control.
  * Removing useless docs, don't add worthwhile new information.
  * Reverting unneeded upstream modification, using install target of rules.
  * Cleaning up target depends in rules.
  * Correcting copyright file.
  * Correcting typo in copyright.
  * Replacing install target in rules with debhelper install file.
  * Updating to standards 3.8.0.
  * Upgrading package to debhelper 7.
  * Updating vcs fields.
  * Simplifying manpages debhelper file.

 -- Daniel Baumann <daniel@debian.org>  Fri, 19 Sep 2008 08:04:00 +0200
